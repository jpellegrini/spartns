(in-package :jp-utils-test)

(define-test utils)

(define-test test-binary-search 
  :parent utils
  (multiple-value-bind (result found) (binary-search 234  #(1 3 10 100 234 500 1234))
    (is eql result 4)
    (is eql found t))
  (multiple-value-bind (result found) (binary-search 1    #(1 3 10 100 234 500 1234))
    (is eql result 0)
    (is eql found t))
  (multiple-value-bind (result found) (binary-search 1234 #(1 3 10 100 234 500 1234))
    (is eql result 6)
    (is eql found t))
  (multiple-value-bind (result found) (binary-search 9    #(1 3 10 100 234 500 1234) :not-found t)
    (is eql result 2)
    (is eql found nil))
  (multiple-value-bind (result found) (binary-search 99   #(1 3 10 100 234 500 1234) :not-found t)
    (is eql result 3)
    (is eql found nil))
  (multiple-value-bind (result found) (binary-search 233  #(1 3 10 100 234 500 1234) :not-found t)
    (is eql result 4)
    (is eql found nil))
  (multiple-value-bind (result found) (binary-search 250  #(1 3 10 100 234 500 1234) :not-found t)
    (is eql result 5)
    (is eql found nil))
  (multiple-value-bind (result found) (binary-search 501  #(1 3 10 100 234 500 1234) :not-found t)
    (is eql result 6)
    (is eql found nil))
  (multiple-value-bind (result found) (binary-search 234  #(1 3 10 100 234 500 1234) :test= #'equal)
    (is eql result 4)
    (is eql found t))
  (multiple-value-bind (result found) (binary-search 3    #(1 3 10 100 234 500 1234) :start 3 :not-found -12.1)
    (is eql result -12.1)
    (is eql found nil))
  (multiple-value-bind (result found) (binary-search 3    #(1 3 10 100 234 500 1234) :start 1 :end 0 :not-found 'oranges)
    (is eql result 'oranges)
    (is eql found nil))
  (multiple-value-bind (result found) (binary-search 3    #(1 3 10 100 234 500 1234) :start 1)
    (is eql result 1)
    (is eql found t))
  (multiple-value-bind (result found) (binary-search 3    #(1234 500 234 100 10 3 1) :test< #'>)
    (is eql result 5)
    (is eql found t))
  (multiple-value-bind (result found) (binary-search #\f  "abcdefghijkl" :get-element #'aref)
    (is eql result 5)
    (is eql found t))
  t)


(define-test test-nonrec-binary-search 
  :parent utils
  (multiple-value-bind (result found) (nonrec-binary-search 234  #(1 3 10 100 234 500 1234))
    (is eql result 4)
    (is eql found t))
  (multiple-value-bind (result found) (nonrec-binary-search 1    #(1 3 10 100 234 500 1234))
    (is eql result 0)
    (is eql found t))
  (multiple-value-bind (result found) (nonrec-binary-search 1234 #(1 3 10 100 234 500 1234))
    (is eql result 6)
    (is eql found t))
  (multiple-value-bind (result found) (nonrec-binary-search 9    #(1 3 10 100 234 500 1234) :not-found t)
    (is eql result 2)
    (is eql found nil))
  (multiple-value-bind (result found) (nonrec-binary-search 99   #(1 3 10 100 234 500 1234) :not-found t)
    (is eql result 3)
    (is eql found nil))
  (multiple-value-bind (result found) (nonrec-binary-search 233  #(1 3 10 100 234 500 1234) :not-found t)
    (is eql result 4)
    (is eql found nil))
  (multiple-value-bind (result found) (nonrec-binary-search 250  #(1 3 10 100 234 500 1234) :not-found t)
    (is eql result 5)
    (is eql found nil))
  (multiple-value-bind (result found) (nonrec-binary-search 501  #(1 3 10 100 234 500 1234) :not-found t)
    (is eql result 6)
    (is eql found nil))
  (multiple-value-bind (result found) (nonrec-binary-search 234  #(1 3 10 100 234 500 1234) :test= #'equal)
    (is eql result 4)
    (is eql found t))
  (multiple-value-bind (result found) (nonrec-binary-search 3    #(1 3 10 100 234 500 1234) :start 3 :not-found -12.1)
    (is eql result -12.1)
    (is eql found nil))
  (multiple-value-bind (result found) (nonrec-binary-search 3    #(1 3 10 100 234 500 1234) :start 1 :end 0 :not-found 'oranges)
    (is eql result 'oranges)
    (is eql found nil))
  (multiple-value-bind (result found) (nonrec-binary-search 3    #(1 3 10 100 234 500 1234) :start 1)
    (is eql result 1)
    (is eql found t))
  (multiple-value-bind (result found) (nonrec-binary-search 3    #(1234 500 234 100 10 3 1) :test< #'>)
    (is eql result 5)
    (is eql found t))
  (multiple-value-bind (result found) (nonrec-binary-search #\f  "abcdefghijkl" :get-element #'aref)
    (is eql result 5)
    (is eql found t))
  t)


(define-test test-get-binary-search
  :parent utils
  (let ((bsearch-fixnum     (get-binary-search :element-type fixnum))
	(bsearch-fixnum-pos (get-binary-search :element-type fixnum :not-found t))
	(bsearch-double     (get-binary-search :element-type double-float))
	(bsearch-double-pos (get-binary-search :element-type double-float :not-found t))
	(bsearch-char       (get-binary-search :element-type  character))
	;; apparently, SBCL does not understand the type of the MAKE-ARRAY below as
	;; the same as the one expected by the GET-BINARY-SEARCH function with fixnum
	;; type.
;;	(vector-fixnum      (make-array  7 :element-type 'fixnum
;;					 :adjustable nil
;;					 :initial-contents #(1 3 10 100 234 500 1234)))
;;	                                 :start 1 :end 0)
	(vector-fixnum      (coerce #(1 3 10 100 234 500 1234)
				    '(simple-array fixnum (*))))
	(vector-double      (make-array  7 :element-type 'double-float
					 :adjustable nil
					 :initial-contents #(1d0 3d0 10d0 100d0 234d0 500d0 1234d0)))
	(vector-character   (make-array 12 :element-type 'character
					:adjustable nil
					:initial-contents "abcdefghijkl")))
    (multiple-value-bind (result found) (funcall bsearch-fixnum     234   vector-fixnum)
      (is eql result 4)
      (is eql found t))
    (multiple-value-bind (result found) (funcall bsearch-fixnum     1     vector-fixnum)
      (is eql result 0)
      (is eql found t))
    (multiple-value-bind (result found) (funcall bsearch-fixnum     1234  vector-fixnum)
      (is eql result 6)
      (is eql found t))
    (multiple-value-bind (result found) (funcall bsearch-fixnum-pos 9     vector-fixnum)
      (is eql result 2)
      (is eql found nil))
    (multiple-value-bind (result found) (funcall bsearch-fixnum-pos 99    vector-fixnum)
      (is eql result 3)
      (is eql found nil))
    (multiple-value-bind (result found) (funcall bsearch-fixnum-pos 233   vector-fixnum)
      (is eql result 4)
      (is eql found nil))
    (multiple-value-bind (result found) (funcall bsearch-double-pos 250d0 vector-double)
      (is eql result 5)
      (is eql found nil))
    (multiple-value-bind (result found) (funcall bsearch-char       #\f   vector-character)
      (is eql result 5)
      (is eql found t))
    (multiple-value-bind (result found) (funcall bsearch-fixnum 3   (coerce #(1 3 10 100 234 500 1234 2000)
									    '(simple-array fixnum (*)))
									    :start 1 :end 0)
    (is eql result nil)
    (is eql found nil)))
  t)


(define-test test-hash-table-keys
  :parent utils
  (loop repeat 10 do
    (let* ((b (make-hash-table))
	   (k 0)
	   (keylist (loop repeat 60 do
	     (setf k (random 10000))
	     (setf (gethash k b) (random 10))
			  collect k)))
      (is equalp (remove-duplicates (sort keylist #'<))
		     (remove-duplicates (sort (hash-table-keys b) #'<))))))


(define-test test-counting-sort
  :parent utils
  (multiple-value-bind (sorted count-array)
      (counting-sort (make-array 16 
				 :element-type 'fixnum
				 :initial-contents '(1 3 5 5 4 8 1 2 2 9 1 1 3 0 3 4))
		     10 :start 0 :end 15)
    (is equalp sorted #(0 1 1 1 1 2 2 3 3 3 4 4 5 5 8 9))
    (is equalp count-array #(0 1 5 7 10 12 14 14 14 15)))
  (dotimes (i 50)
    (let* ((size (1+ (random 1000))) ;; the "1+" is there because (aref #() 0) wouldn't work
	   (a (make-array size :element-type 'fixnum :adjustable nil))
	   (max-element (random 10000)))
      (dotimes (j size)
	(setf (aref a j) (random max-element)))
      (let ((sorted (counting-sort a max-element)))
	(true (sortedp sorted))
	(dotimes (j size)
	  (true (< (aref sorted j) max-element)))))))


(define-test test-counting-sort-all
  :parent utils
  (loop repeat 10 do
    (let* ((a  (generate-random-array 1000 0 10))
	   (b  (make-array 1000 :element-type 'fixnum :adjustable nil :initial-contents a))
	   (c  (make-array 1000 :element-type 'fixnum :adjustable nil :initial-contents a))
	   (sa nil)
	   (sb nil)
	   (sc (make-array 1000 :element-type 'fixnum :adjustable nil)))
      (setf sa (counting-sort a 10))
      (counting-sort-g c sc 10 0 999)
      (setf sb (sort b #'<))
      (true (sortedp sa))
      (true (sortedp sb))
      (true (sortedp sc))
      (is equalp sa sb)
      (is equalp sa sc))))


The script test-across-lisps.pl runs tests across implementations.
You can add more implementations there if you want.

Currently recommended:
----------------------

The fastest platform for Spartns seems to be SBCL, although
all others seem to run fine (but do run the "do-benchmarks.lisp"
file on your implementation before deciding to use it -- some
implementations are very slow for some representation schemes).


Current state:
--------------

```
ABCL               works
ACL                works
Clasp              works
CLISP              works
Clozure            works
CMUCL              works (tested the 32bit binary on a 64bit platform; passes all tests)
Corman             ?
ECL                works (version from git)
GCL                works - git checkout (benchmarks need tuning; tests do not run)
LispWorks          ?
Mankai             works
SBCL               works
XCL                probably works (latest version did pass all tests, but XCL is currently not maintained)
```

* I could not test Corman Common Lisp, as I do not have Windows installed.
* Spartns seems to work on GCL checked out from git, but needs tuning in
  order to run the benchmarks. If it fails as this:

```
     Signalled by PROGN.
     Condition in PROGN [or a callee]: INTERNAL-SIMPLE-ERROR: The assertion v && (unsigned long)(v+sz)<MAX_CODE_ADDRESS
      on line 1035 of alloc.c in function alloc_code_space
      failed: Success
 ```

then some tuning may be necessary (`GCL_MEM_MULTIPLE`, `GCL_GC_PAGE_THRESH`, `GCL_GC_ALLOC_MIN`,
`GCL_GC_PAGE_MAX`, `GCL_MULTIPROCESS_MEMORY_POOL`), and it is still possible to allocate clde 
block space inside the program, setting the variable `*code-block-reserve*` (see the beginning 
of file `benchmark.lisp`).



Workarounds:
------------

* For some reason doing `(aref M 2)` (or any other number) in
  Clozure CL doesn't seem to work, so we have to do `(coerce 2 'fixnum)`.
  That probably has an impact on performance.
  This only afects the `ARRAY` scheme on Cozure, and only the `GET`
  function (`SET` and traversals are not affected)
* In Allegro Common Lisp, if A is a `SIMPLE-ARRAY`, the result of
  `(adjust-array A new-size)` is not.
  Spartns now uses a function called `ROBUST-ADJUST-ARRAY` for this.
  ACL is treated as a special case.
* As a consequence of adjusting non-adjustable arrays (as mentioned
  in the previous points), growing a `CVECTOR` beyond its capacity
  can be fast or expensive, depending on the Common Lisp implementation
  (if your Lisp makes a new array and copies everything over, it will
  be O(n); if it is smart enough to optimize this then it can be done in
  O(1))

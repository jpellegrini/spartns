# Spartns

Spartns is a SPARse TeNSor representation library. If you don't know what a tensor is: they are objects from Multilinear Algebra, which can be represented
as matrices, but with any number of dimensions, not just two. Think of arrays, like `A[i][j][k][l][m][n][o][p][q]`, for example. 
Spartns is distributed under the LLGPL license.

Features:

*    No external dependencies (no BLAS or any other C or Fortran library needed). Just plain Common Lisp;
*    Represents mappings from one dimension onto another using any scheme you want (there are three built-in schemes: array, hash and compressed-vector, but you can roll your own and plug it);
*    Flexible: works with any data type;
*    Heavily optimized: traversing the tensor can be extremely fast (in one specific situation -- traversing the tensor -- it was 10 times faster than a naive implementation in C++);
*    Fairly portable: works with ABCL, ACL, Clasp, Clisp, Clozure, CMUCL,  ECL, GCL, SBCL, MKCL and XCL;
*    Spartns is never released without going through regression tests (if a platform breaks and can't be supported, it will be clear in the release announcement);
*    ASDF installable (thanks Slobodan Blazeski!);
*    Easy to use, with introductory documentation (not only on-line);
*    Comes with description of the internals of the library.

One simple example: two dimensional matrix multiplication. First, define a type for 2-dimensional matrices:

```
(defspartn 2dmatrix
   :representation (spartns:hash spartns:cvector)
   :non-zero       (3 4)
   :element-type   long-float
   :sparse-element 0L0)
```

The Spartn type "`2dmatrix`" is then defined as the type for sparse tensors that map indices onto long-floats using a hashtable of compressed vectors. When they are created, the hashtables start with `:size 3`,
and the compressed vectors with `:size 4`. Now, create three matrices, X Y and Z, and multiply them:

```
(let ((X (make-2dmatrix))
      (Y (make-2dmatrix))
      (Z (make-2dmatrix)))
  (set-2dmatrix X 0 0 5L0)
  (set-2dmatrix Y 0 1 6L4)
  ;; set non-zeros in the rest of the matrices X and Y

  ;; and now multiply them:
  (w/fast-traversals ((2dmatrix X i j val-x)
                      (2dmatrix Y j k val-y))
    (set-2dmatrix Z i k
      (+ (get-2dmatrix Z i k) (* val-x val-y)))))
```

Some benchmarks (not to be taken too seriously -- just to help understand the performance difference between
representation scheemes and also between Common Lisp implementations).



|               |SBCL	|GCL	|CCL64|      Clisp|      ABCL|  ECL| MKCL|   Clasp| Allegro|
|---------------|-------|-----|-----|-----------|----------|-----|-----|--------|--------|
| ARRAY GET     | 0.02	| 2.75| 0.14|	  1.34|	 0.76| 0.97| 0.85|    1.74|       x|
| ARRAY SET     | 0.08	| 0.00| 0.26|	  2.88|	 1.37| 0.27| 1.78|    3.02|       x|
| CVECTOR GET   | 1.99	| 1.79|14.27|	177.98|	22.87|17.32|14.68|   29.39|    6.77|
| CVECTOR SET   | 2.04	| 7.48|14.45|	175.47|	24.53|19.10|14.48|   30.05|    5.49|
| TR-CVECTOR-GET| 0.14	|19.04| 0.35|	 10.55|	 7.33| 7.44| 4.41|    4.51|   76.12|
| TR-CVECTOR-SET| 0.19	|11.86| 0.33|	 17.59|	 7.89| 7.57| 3.32|    4.38|   75.68|
| HASH-GET      | 0.76	| 4.59| 4.85|	 14.49|	 5.30| 2.54| 2.22|    7.19|       x|
| HASH-SET      | 2.01	| 4.69| 6.32|	 13.09|	 6.15| 1.78| 4.49|    7.37|       x|
| TR-HASH-GET   | 0.04	| 4.90| 5.97|	  8.11|	 4.74| 2.19| 2.71|   10.63|       x|
| TR-HASH-SET   | 1.85	| 4.80| 7.07|	  7.92|	 5.43| 2.41| 3.75|   10.69|       x|


```
C++ STL GET	2.01
C++ STL SET	1.96
```

The C++ timings were obtained with the (extremely simple) benchmark in the
`.cpp` files.

* ACL Free Express has a heap limit and did not finish the benchmark.
* GCL results will depend on tuning some environment variables (`GCL_MEM_MULTIPLE`,
  `GCL_GC_PAGE_THRESH`, `GCL_GC_ALLOC_MIN`, `GCL_GC_PAGE_MAX`, `GCL_MULTIPROCESS_MEMORY_POOL`),
  and it is still possible to allocate clde block space inside the program,
  setting the variable `*code-block-reserve*` (see the beginning of file
  `benchmark.lisp`).

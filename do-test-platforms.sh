#!/bin/bash
#
# This script runs tests on all supported platforms
#

echo -n "tests: start... "
echo "- - - - -" > test-results.txt

echo -n " [SBCL] "
echo "SBCL: " >> test-results.txt
# --script does not load parachute
sbcl --load do-tests.lisp \
     2>test-SBCL.log  \
    | grep 'tests>' >> test-results.txt  
echo "- - - - -" >> test-results.txt

echo -n " [Clisp] "
echo "Clisp: " >> test-results.txt
# use pipe, otherwise Clisp gets confused about packages
clisp < do-tests.lisp \
      2>test-clisp.log \
    | grep 'tests>' >> test-results.txt 
echo "- - - - -" >> test-results.txt

echo -n " [ECL] "
echo "ECL: " >> test-results.txt
ecl -load do-tests.lisp \
    2>test-ecl.log \
    | grep 'tests>' >> test-results.txt
echo "- - - - -" >> test-results.txt

# Could not load Parachute in GCL.
# echo " [GCL] "
# GCL_ANSI=1 gcl -load 
# echo "- - - - -" >> test-results.txt

echo -n " [CCL] "
echo "CCL: " >> test-results.txt
ccl64 --load /home/jeronimo/quicklisp/setup.lisp -e '(progn (ql:quickload "parachute") (load "do-tests.lisp"))'\
      2>test-ccl.log \
    | grep 'tests>' >> test-results.txt
echo "- - - - -" >> test-results.txt

echo -n " [ABCL] "
echo "ABCL: " >> test-results.txt
java -jar /home/jeronimo/pkg/abcl/dist/abcl.jar --batch --load do-tests.lisp \
     2> test-abcl.log \
    | grep 'tests>' >> test-results.txt
echo "- - - - -" >> test-results.txt

# Clasp fails to build on my machine
echo -n " [Clasp] "
echo "Clasp: " >> test-results.txt
clasp --load do-tests.lisp \
      2> test-clasp.log \
    | grep 'tests>' >> test-results.txt
echo "- - - - -" >> test-results.txt

# XCL is currently not buildable
# ["XCL"]="/home/jeronimo/pkg/lisp/xcl/xcl --load do-tests.lisp"

echo "done."


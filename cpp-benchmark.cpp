#include <map>
#include <iostream>
#include "timer.h"

int main () {
	std::map<unsigned int, std::map<unsigned int, std::map<unsigned int, double> > > mat;
	unsigned a, i, j, k;
	double x = 9.3;
	double s = 0.0;

	Timer t;

	for (i=1; i<90; i++)
		for (j=1; j<90; j++)
			for (k=0; k<90; k++)
				mat[i][j][k] = x;
	std::cout << "start...\n";
	t.start();
	for (a=0; a<50; a++)
                for (i=1; i<90; i++)
                        for (j=1; j<90; j++)
                                for (k=0; k<90; k++)
                                        mat[i][j][k] = x;
	t.stop();

	// Calculate and print sum, so the compiler won't completely
	// delete the loop:
	for (i=1; i<90; i++)
		for (j=1; j<90; j++)
			for (k=0; k<90; k++)
				s += mat[i][j][k];
	std::cout << "Matrix sum: " << s << "\n";

	std::cout << "Elapsed time (SET): "
	          << t.elapsed_time()
		  << " seconds\n";

	t.reset();
        t.start();
        for (a=0; a<50; a++)
                for (i=1; i<90; i++)
                        for (j=1; j<90; j++)
                                for (k=0; k<90; k++)
                                        x = mat[i][j][k];
        t.stop();

	// Print x so the compiler won't completely delete the loop:
	std::cout << "x = " << x << "\n";

        std::cout << "Elapsed time (GET): "
                  << t.elapsed_time()
                  << " seconds\n";

}

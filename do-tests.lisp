(load "spartns-packages.lisp")
(load "spartns-test-packages.lisp")
(load "utils.lisp")
(load "spartns.lisp")
(load "utils-tests.lisp")
(load "tests.lisp")

;; parachute supports three kinds of report:
;; - plain, in which each test produces an
;;   output and returns a report object
;; - quiet, which only returns an object
;;   which holds the number of tests performed
;;   and wether there were any failures
;; - interactive, in which test failures will
;;   trigger errors

(in-package :jp-utils-test)
(defvar utils-report nil)
(setf utils-report (parachute::test 'utils :report 'quiet))

(in-package :spartns-test)
(defvar main-report nil)
(setf main-report (parachute::test 'main :report 'quiet))

(princ '-----------------------------------------)
(terpri)
(in-package :common-lisp-user)
(format T "tests> utils: ~a~%" (parachute::summarize jp-utils-test::utils-report))
(format T "tests> main: ~a~%" (parachute::summarize spartns-test::main-report))

#-clasp (quit)
#+clasp (core:quit)
